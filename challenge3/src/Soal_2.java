import java.util.Arrays;

public class Soal_2 {
    public static void main(String[] args) {
        int[] num = {2, 3, 1, 4, 4, 5, 6, 6};
        int lenNewNum = num.length;
        int result = lenNewNum - 1;
        System.out.println("Tentukan angka terbesar dalam array berikut");
        System.out.println(Arrays.toString(num));
        System.out.println("Jika index angka terbesar lebih dari satu maka akan diganti dengan angka terbesar lainnya");

        for (int i = 0; i < lenNewNum; i++)
        {
            for (int j = 0; j < lenNewNum; j++)
            {
                if (i == j) {
                    continue;
                }
                if ( num[i] == num[j]) {
                    if (num[i] != 0 && num[j] != 0)
                    {
                        num[i] = 0;
                        num[j] = 0;
                    }
                }
            }
        }

        Arrays.sort(num);
        System.out.println("Angka terbesar dalam array tersebut adalah : " + num[result]);

    }
}
