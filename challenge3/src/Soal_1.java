public class Soal_1 {
    public static void main(String[] args) {
        int[] num = {1, 2, 3, 4, 5, 6};
        int len  = num.length;

        System.out.println("bilangan ganjil : ");
        for (int i = 0; i < len; i++)
        {
            if (num[i] %2 != 0)
            {
                System.out.println(num[i]);
            }
        }

        System.out.println("bilangan genap : ");
        for (int i = 0; i < len; i++)
        {
            if (num[i] %2 == 0 )
            {
                System.out.println(num[i]);
            }
        }

    }
}