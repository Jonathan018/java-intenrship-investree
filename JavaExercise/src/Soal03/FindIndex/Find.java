package Soal03.FindIndex;

public class Find {
    public int panjangArray;
    public int inputUser;

    public Integer a;

    public Find(){}

    public Find(int paramPanjangArray, int paramInputanUser){
        this.panjangArray = paramPanjangArray;
        this.inputUser = paramInputanUser;
    }

    public Integer index(){
        if (inputUser < panjangArray && inputUser >= 0){
            return inputUser;
        } else {
            return a;
        }
    }

    public Integer index(int paramPanjangArray, int paramInputanUser){
        this.panjangArray = paramPanjangArray;
        this.inputUser = paramInputanUser;

        if (inputUser < panjangArray && inputUser >= 0){
            return inputUser;
        } else {
            return a;
        }
    }
}
