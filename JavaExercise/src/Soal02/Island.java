package Soal02;

import Soal02.Move.Move;
import Soal02.Move.PullOver;

import java.util.Scanner;

public class Island {
    public static void main(String[] args) {
        Scanner myScan = new Scanner(System.in);

        int[] numMap = {1, 2, 3 , 4 , 5, 6, 7, 8, 9, 10};
        int panjangNumMap = numMap.length;
        String[] charMap = {"A","B","C","D","E","F","G","H","I","J" };
        int panjangcharMap = charMap.length;

        Integer kordinatX = 4;
        Integer kordinatY = 5;
        PullOver pullOver = new PullOver(panjangNumMap,panjangcharMap,kordinatX,kordinatY);
        Move move = new Move(panjangNumMap, panjangcharMap, kordinatX, kordinatY);

        int masukanUser;
        boolean perulangan = true;
        System.out.println("Posisi Anda Saat ini berada di "+ charMap[kordinatY] + numMap[kordinatX]);

        while (perulangan) {
            System.out.println("Apa yang ingin anda inginkan?");
            System.out.println("1. Move");
            System.out.println("2. Pull Over");
            System.out.println("3. Restart");
            System.out.println("4. Exit");
            System.out.print("Pilih : ");
            masukanUser = myScan.nextInt();

            switch (masukanUser) {
                case 1:
                    System.out.println("Anda berada di menu Move");
                    System.out.println("Ke arah mana karakter berpindah ?");
                    System.out.println("  N");
                    System.out.print("W ");
                    System.out.println("  E");
                    System.out.println("  S");
                    System.out.print("Pilih : ");
                    String arahMove = myScan.next();

                    System.out.println("Seberapa jauh berpindah ?");
                    System.out.print("jumlah : ");
                    int jumlahMove = myScan.nextInt();

                    arahMove = String.valueOf(arahMove.toUpperCase().trim().charAt(0));
                    Integer penampung;

                    switch (arahMove) {
                        case "N":
                            penampung = move.bergerak(1, jumlahMove);
                            if (penampung == null) {
                                System.out.println("Index yang anda masukkan melebihi jumlah index yang dimiliki");
                            } else {
                                kordinatY = penampung;
                                System.out.println("Maka posisi anda saat ini berada di " + charMap[kordinatY] + numMap[kordinatX]);
                            }
                            break;
                        case "S":
                            penampung = move.bergerak(2,jumlahMove);
                            if (penampung == null) {
                                System.out.println("Index yang anda masukkan tidak sesuai dengan jumlah index yang dimiliki");
                            } else {
                                kordinatY = penampung;
                                System.out.println("Maka posisi anda saat ini berada di " + charMap[kordinatY] + numMap[kordinatX]);
                            }
                            break;
                        case "E" :
                            penampung = move.bergerak(3, jumlahMove);
                            if (penampung == null) {
                                System.out.println("Index yang anda masukkan melebihi jumlah index yang dimiliki");
                            } else {
                                kordinatX = penampung;
                                System.out.println("Maka posisi anda saat ini berada di " + charMap[kordinatY] + numMap[kordinatX]);
                            }
                            break;
                        case "W" :
                            penampung = move.bergerak(4, jumlahMove);
                            if (penampung == null) {
                                System.out.println("Index yang anda masukkan tidak sesuai dengan jumlah index yang dimiliki");
                            } else {
                                kordinatX = penampung;
                                System.out.println("Maka posisi anda saat ini berada di " + charMap[kordinatY] + numMap[kordinatX]);
                            }
                            break;
                        default:
                            System.out.println("Anda memasukkan format arah yang salah");
                    }
                    break;
                case 2:
                    System.out.println("Anda berada di menu PullOver");
                    System.out.println("Ke arah mana karakter berpindah ?");
                    System.out.println("  N");
                    System.out.print("W ");
                    System.out.println("  E");
                    System.out.println("  S");
                    System.out.print("Pilih : ");
                    String arahMove2 = myScan.next();

                    System.out.println("Seberapa jauh berpindah ?");
                    System.out.print("jumlah : ");
                    int jumlahMove2 = myScan.nextInt();

                    arahMove2 = String.valueOf(arahMove2.toUpperCase().trim().charAt(0));
                    Integer penampung2;

                    switch (arahMove2) {
                        case "N":
                            penampung2 = pullOver.bergerak(1, jumlahMove2);
                            if (penampung2 == null) {
                                System.out.println("Index yang anda masukkan melebihi jumlah index yang dimiliki");
                            } else {
                                kordinatY = penampung2;
                                System.out.println("Maka posisi anda saat ini berada di " + charMap[kordinatY] + numMap[kordinatX]);
                            }
                            break;
                        case "S":
                            penampung2 = pullOver.bergerak(2,jumlahMove2);
                            if (penampung2 == null) {
                                System.out.println("Index yang anda masukkan tidak sesuai dengan jumlah index yang dimiliki");
                            } else {
                                kordinatY = penampung2;
                                System.out.println("Maka posisi anda saat ini berada di " + charMap[kordinatY] + numMap[kordinatX]);
                            }
                            break;
                        case "E" :
                            penampung2 = pullOver.bergerak(3, jumlahMove2);
                            if (penampung2 == null) {
                                System.out.println("Index yang anda masukkan melebihi jumlah index yang dimiliki");
                            } else {
                                kordinatX = penampung2;
                                System.out.println("Maka posisi anda saat ini berada di " + charMap[kordinatY] + numMap[kordinatX]);
                            }
                            break;
                        case "W" :
                            penampung2 = pullOver.bergerak(4, jumlahMove2);
                            if (penampung2 == null) {
                                System.out.println("Index yang anda masukkan tidak sesuai dengan jumlah index yang dimiliki");
                            } else {
                                kordinatX = penampung2;
                                System.out.println("Maka posisi anda saat ini berada di " + charMap[kordinatY] + numMap[kordinatX]);
                            }
                            break;
                        default:
                            System.out.println("Anda memasukkan format arah yang salah");
                    }
                    break;
                case 3 :
                    pullOver.kordinatY = 5;
                    pullOver.kordinatX = 4;
                    break;
                case 4:
                    perulangan = false;
                default:
                    System.out.println("Pilihan Yang anda masukkan tidak tersedia");

            }
        }


    }


}
