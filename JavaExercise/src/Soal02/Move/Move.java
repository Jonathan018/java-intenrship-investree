package Soal02.Move;

import javax.security.auth.callback.LanguageCallback;

public class Move {
    public int batasKordinatX;
    public int batasKordinatY;
    public Integer kordinatX;
    public Integer kordinatY;

    public Move(int paramBatasKordinatX, int paramBatasKordinatY, int paramKordinatX, int paramKordinatY ){
        this.batasKordinatX = paramBatasKordinatX - 1;
        this.batasKordinatY = paramBatasKordinatY - 1;
        this.kordinatX = paramKordinatX;
        this.kordinatY = paramKordinatY;
    }

    public Integer bergerak(int paramArah, int paramJumlahLangkah){
        Integer keluaran = null;
        switch (paramArah){
            case 1 :
                keluaran = bergerakArahAtas(paramJumlahLangkah);
                break;
            case 2 :
                keluaran = bergerakArahBawah(paramJumlahLangkah);
                break;
            case 3 :
                keluaran = bergerakArahKanan(paramJumlahLangkah);
                break;
            case 4 :
                keluaran = bergerakArahKiri(paramJumlahLangkah);
                break;
        }
        if (keluaran != null){
            if (paramArah > 0 && paramArah < 3){
                this.kordinatY = keluaran;
            }else if(paramArah >= 3 && paramArah <= 4){
                this.kordinatX = keluaran;
            }
        }
        return keluaran;
    }

    private Integer bergerakArahAtas(int jumlahLangkah){
        Integer hasilArahAtas = null;
        int tujuanAkhir = jumlahLangkah + this.kordinatY;
        if(tujuanAkhir <= this.batasKordinatY && jumlahLangkah >= 0){
            hasilArahAtas = tujuanAkhir;
        }
        return hasilArahAtas;
    }

    private Integer bergerakArahBawah(int jumlahLangkah){
        Integer hasilArahBawah = null;
        int tujuanAkhir = this.kordinatY - jumlahLangkah;
        if (tujuanAkhir >= 0 && jumlahLangkah >= 0){
            hasilArahBawah = tujuanAkhir;
        }
        return hasilArahBawah;
    }

    private Integer bergerakArahKanan(int jumlahLangkah){
        Integer hasilArahKanan = null;
        int tujuanAkhir = jumlahLangkah + this.kordinatX;
        if (tujuanAkhir <= this.batasKordinatX && jumlahLangkah >= 0){
            hasilArahKanan = tujuanAkhir;
        }
        return hasilArahKanan;
    }

    private Integer bergerakArahKiri(int jumlahLangkah){
        Integer hasilArahKiri = null;
        int tujuanAkhir = this.kordinatX - jumlahLangkah;
        if (tujuanAkhir >= 0 && jumlahLangkah >= 0){
            hasilArahKiri = tujuanAkhir;
        }
        return hasilArahKiri;
    }
}
