package Soal02.Move;

public class PullOver {
        public int batasKordinatX;
        public int batasKordinatY;
        public Integer kordinatX;
        public Integer kordinatY;

        public PullOver(int paramBatasKordinatX, int paramBatasKordinatY, int paramKordinatX, int paramKordinatY ){
            this.batasKordinatX = paramBatasKordinatX - 1;
            this.batasKordinatY = paramBatasKordinatY - 1;
            this.kordinatX = paramKordinatX;
            this.kordinatY = paramKordinatY;
        }

        public Integer bergerak(int paramArah, int paramJumlahLangkah){
            Integer keluaran = null;
            switch (paramArah){
                case 1 :
                    keluaran = bergerakArahAtas(paramJumlahLangkah);
                    break;
                case 2 :
                    keluaran = bergerakArahBawah(paramJumlahLangkah);
                    break;
                case 3 :
                    keluaran = bergerakArahKanan(paramJumlahLangkah);
                    break;
                case 4 :
                    keluaran = bergerakArahKiri(paramJumlahLangkah);
                    break;
            }
            if (keluaran != null){
                if (paramArah >= 1 && paramArah <=2){
                    this.kordinatY = keluaran;
                }else if(paramArah >= 3 && paramArah <= 4){
                    this.kordinatX = keluaran;
                }
            }
            return keluaran;
        }

        private Integer bergerakArahAtas(int jumlahLangkah){
            Integer hasilArahAtas = null;
            int tujuanAkhir = jumlahLangkah + this.kordinatY;
            if(tujuanAkhir <= this.batasKordinatY ){
                hasilArahAtas = tujuanAkhir;
            }else {
                int penampungKordinatY = this.kordinatY;
                for (int i = 1; i <= jumlahLangkah; i++){
                    if (penampungKordinatY < this.batasKordinatY){
                        penampungKordinatY += 1;
                    }else{
                        penampungKordinatY = 0;
                    }
                    hasilArahAtas = penampungKordinatY;
                }
            }
            return hasilArahAtas;
        }

        private Integer bergerakArahBawah(int jumlahLangkah){
            Integer hasilArahBawah = null;
            int tujuanAkhir = this.kordinatY - jumlahLangkah;
            if (tujuanAkhir >= 0 && jumlahLangkah >= 0){
                hasilArahBawah = tujuanAkhir;
            }else {
                int penampungKordinatY = this.kordinatY;
                for (int i = 1; i <= jumlahLangkah; i++){
                    if (penampungKordinatY > 0){
                        penampungKordinatY -= 1;
                    }else{
                        penampungKordinatY = this.batasKordinatY;
                    }
                    hasilArahBawah = penampungKordinatY;
                }
            }
            return hasilArahBawah;
        }

        private Integer bergerakArahKanan(int jumlahLangkah){
            Integer hasilArahKanan = null;
            int tujuanAkhir = jumlahLangkah + this.kordinatX;
            if (tujuanAkhir <= this.batasKordinatX && jumlahLangkah >= 0){
                hasilArahKanan = tujuanAkhir;
            }else {
                int penampungKordinatX = this.kordinatX;
                for (int i = 1; i <= jumlahLangkah; i++){
                    if (penampungKordinatX < this.batasKordinatX){
                        penampungKordinatX += 1;
                    }else{
                        penampungKordinatX = 0;
                    }
                    hasilArahKanan = penampungKordinatX;
                }
            }

            return hasilArahKanan;
        }

        private Integer bergerakArahKiri(int jumlahLangkah){
            Integer hasilArahKiri = null;
            int tujuanAkhir = this.kordinatX - jumlahLangkah;
            if (tujuanAkhir >= 0 && jumlahLangkah >= 0){
                hasilArahKiri = tujuanAkhir;
            }else {
                int penampungKordinatX = this.kordinatX;
                for (int i = 1; i <= jumlahLangkah; i++){
                    if (penampungKordinatX > 0){
                        penampungKordinatX -= 1;
                    }else{
                        penampungKordinatX = this.batasKordinatX;
                    }
                    hasilArahKiri = penampungKordinatX;
                }
            }

            return hasilArahKiri;
        }
    }
