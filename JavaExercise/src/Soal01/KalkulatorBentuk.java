package Soal01;
import Soal01.BangunDatar.Segitiga;
import Soal01.BangunRuang.Balok;
import Soal01.BangunRuang.Kubus;

import java.util.Scanner;


public class KalkulatorBentuk {
    public static void main(String[] ar2gs) {

        Segitiga segitiga = new Segitiga();
        Balok balok = new Balok();
        Kubus kubus = new Kubus();

        int masukanUser;
        Scanner myscan = new Scanner(System.in);

        System.out.println("Pilih jenis kalkulator");
        System.out.println("1. Luas segitiga");
        System.out.println("2. Volume balok");
        System.out.println("3. Volume kubus");
        System.out.print("Pilihan anda : ");

        masukanUser = myscan.nextInt();

        switch (masukanUser){
            case 1 :
                System.out.print("Alas Segitiga : ");
                double alasSegitiga = myscan.nextDouble();
                System.out.print("Tinggi Segitiga : ");
                double tinggiSegitiga = myscan.nextDouble();
                double hasilPerhitunganSegitiga = segitiga.luas(alasSegitiga, tinggiSegitiga);
                System.out.println("Luas Segitiga = " + hasilPerhitunganSegitiga);
                break;
            case 2 :
                System.out.print("Panjang Balok : ");
                double panjangBalok = myscan.nextDouble();
                System.out.print("Lebar Balok : ");
                double lebarBalok = myscan.nextDouble();
                System.out.print("Tinggi Balok : ");
                double tinggiBalok = myscan.nextInt();
                double hasilPerhitunganBalok = balok.volume(panjangBalok,lebarBalok,tinggiBalok);
                System.out.println("Volume Balok = " + hasilPerhitunganBalok);
                break;
            case 3 :
                System.out.print("Sisi Kubus : ");
                double sisiKubus = myscan.nextInt();
                double hasilPerhitunganKubus = kubus.volume(sisiKubus);
                System.out.println("Volume Kubus = " + hasilPerhitunganKubus);
                break;
            default:
                System.out.println("Anda Memasukkan input yang salah ");

        }


    }
}
