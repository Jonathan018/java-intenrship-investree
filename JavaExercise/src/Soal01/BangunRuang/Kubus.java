package Soal01.BangunRuang;

public class Kubus {
    public double sisi;
    public double hasil;

    public Kubus(){}

    public Kubus(double paramSisi){
        this.sisi = paramSisi;
    }

    public double volume(){
        hasil = sisi * sisi * sisi;
        return hasil;
    }

    public double volume(double paramSisi){
        this.sisi = paramSisi;
        hasil = sisi * sisi * sisi;
        return hasil;
    }
}
