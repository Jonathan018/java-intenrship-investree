package Soal01.BangunRuang;

public class Balok {
    public double panjang;
    public double lebar;
    public double tinggi;
    public double hasil;

    public Balok(){}

    public Balok(double paramPanjang, double paramLebar, double paramTinggi){
        this.panjang = paramPanjang;
        this.lebar = paramLebar;
        this.tinggi = paramTinggi;
    }

    public double volume(){
        hasil = panjang * lebar * tinggi;
        return hasil;
    }

    public double volume(double paramPanjang, double paramLebar, double paramTinggi){
        hasil = paramPanjang * paramLebar * paramTinggi;
        return hasil;
    }

}
